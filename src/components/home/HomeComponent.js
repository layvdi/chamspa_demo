import {
  Paper,
  Grid,
  Typography,
  Table,
  TableHead,
  TableCell,
  TableRow,
  TableBody
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import clsx from "clsx";
import {
  SupervisorAccount,
  CheckCircle,
  Error,
  PlayCircleOutline
} from "@material-ui/icons";

const useStyles = makeStyles(theme => ({
  paper: {
    display: "flex",
    minHeight: "10rem",
    justifyContent: "space-between",
    color: "white",
    padding: theme.spacing(2),
    alignItems: "center"
  },
  paperTotalUser: {
    backgroundColor: "#187da0"
  },
  paperTotalUserOnline: {
    backgroundColor: "#2eadd3"
  },
  paperTotalUserLeave: {
    backgroundColor: "#ffc107"
  },
  paperTotalUserLeaveNoReason: {
    backgroundColor: "#f5302e"
  },
  icon: {
    fontSize: "4rem"
  },
  tableWrapper: {
    marginTop: theme.spacing(5),
    minHeight: "30rem",
    padding: theme.spacing(2)
  },
  viewMore: {
    float: 'right',
    marginTop: '15px',
    textDecoration: 'none'
  }
}));

export default () => {
  const classes = useStyles();

  return (
    <>
      <Grid container spacing={5}>
        <Grid item xs={3}>
          <Paper className={clsx(classes.paper, classes.paperTotalUser)}>
            <div>
              <Typography component="h3" variant="h3">
                100
              </Typography>
              <Typography>Tổng nhân viên</Typography>
            </div>
            <SupervisorAccount className={classes.icon} />
          </Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className={clsx(classes.paper, classes.paperTotalUserOnline)}>
            <div>
              <Typography component="h3" variant="h3">
                90
              </Typography>
              <Typography>Nhân viên đang làm việc</Typography>
            </div>
            <PlayCircleOutline className={classes.icon} />
          </Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className={clsx(classes.paper, classes.paperTotalUserLeave)}>
            <div>
              <Typography component="h3" variant="h3">
                9
              </Typography>
              <Typography>Nhân viên nghỉ phép</Typography>
            </div>
            <CheckCircle className={classes.icon} />
          </Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper
            className={clsx(classes.paper, classes.paperTotalUserLeaveNoReason)}
          >
            <div>
              <Typography component="h3" variant="h3">
                1
              </Typography>
              <Typography>Nhân viên nghỉ không phép</Typography>
            </div>
            <Error className={classes.icon} />
          </Paper>
        </Grid>
      </Grid>
      <Grid container spacing={5}>
        <Grid item xs={6}>
          <Paper className={classes.tableWrapper}>
            <Typography variant="h6">Sinh nhật trong tháng 8 này</Typography>
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell>Tên</TableCell>
                  <TableCell align="right">Bộ Phận</TableCell>
                  <TableCell align="right">Chức vụ</TableCell>
                  <TableCell align="right">Ngày sinh</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell component="th" scope="row">
                    Nguyễn văn A
                  </TableCell>
                  <TableCell align="right">Kế toán</TableCell>
                  <TableCell align="right">Trưởng phòng</TableCell>
                  <TableCell align="right">30/03/1991</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell component="th" scope="row">
                    Nguyễn văn A
                  </TableCell>
                  <TableCell align="right">Kế toán</TableCell>
                  <TableCell align="right">Trưởng phòng</TableCell>
                  <TableCell align="right">30/03/1991</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell component="th" scope="row">
                    Nguyễn văn A
                  </TableCell>
                  <TableCell align="right">Kế toán</TableCell>
                  <TableCell align="right">Trưởng phòng</TableCell>
                  <TableCell align="right">30/03/1991</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell component="th" scope="row">
                    Nguyễn văn A
                  </TableCell>
                  <TableCell align="right">Kế toán</TableCell>
                  <TableCell align="right">Trưởng phòng</TableCell>
                  <TableCell align="right">30/03/1991</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell component="th" scope="row">
                    Nguyễn văn A
                  </TableCell>
                  <TableCell align="right">Kế toán</TableCell>
                  <TableCell align="right">Trưởng phòng</TableCell>
                  <TableCell align="right">30/03/1991</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell component="th" scope="row">
                    Nguyễn văn A
                  </TableCell>
                  <TableCell align="right">Kế toán</TableCell>
                  <TableCell align="right">Trưởng phòng</TableCell>
                  <TableCell align="right">30/03/1991</TableCell>
                </TableRow>
              </TableBody>
            </Table>
            <a href="#" className={classes.viewMore}>Xem thêm ...</a>
          </Paper>
        </Grid>
        <Grid item xs={6}>
          <Paper className={classes.tableWrapper}>
            <Typography variant="h6">3 đơn nghỉ phép cần duyệt</Typography>
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell>Tên</TableCell>
                  <TableCell align="right">Bộ Phận</TableCell>
                  <TableCell align="right">Chức vụ</TableCell>
                  <TableCell align="right">Ngày nghỉ</TableCell>
                  <TableCell align="right">Số ngày nghỉ</TableCell>
                  <TableCell align="right">Ngày tạo</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell component="th" scope="row">
                    Nguyễn văn A
                  </TableCell>
                  <TableCell align="right">Kế toán</TableCell>
                  <TableCell align="right">Trưởng phòng</TableCell>
                  <TableCell align="right">2/09/2019</TableCell>
                  <TableCell align="right">1</TableCell>
                  <TableCell align="right">2/09/2019</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell component="th" scope="row">
                    Nguyễn văn A
                  </TableCell>
                  <TableCell align="right">Kế toán</TableCell>
                  <TableCell align="right">Trưởng phòng</TableCell>
                  <TableCell align="right">2/09/2019</TableCell>
                  <TableCell align="right">1</TableCell>
                  <TableCell align="right">2/09/2019</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell component="th" scope="row">
                    Nguyễn văn A
                  </TableCell>
                  <TableCell align="right">Kế toán</TableCell>
                  <TableCell align="right">Trưởng phòng</TableCell>
                  <TableCell align="right">2/09/2019</TableCell>
                  <TableCell align="right">1</TableCell>
                  <TableCell align="right">2/09/2019</TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};
