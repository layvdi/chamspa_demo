import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';

class ButtonLoading extends Component {
  renderContent = () => {
    if (this.props.isLoading) {
      return (
        <Button
          {...this.props}
          disabled
        >
          <CircularProgress
            size={20}
            color="secondary"
            {...this.props.loading}
          />
          &nbsp;{this.props.messageLoading}
        </Button>
      );
    }

    return (
      <Button
        {...this.props}
      >
        {this.props.children}
      </Button>
    );
  }

  render() {
    return this.renderContent();
  }
}

ButtonLoading.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  loading: PropTypes.object,
  messageLoading: PropTypes.string.isRequired,
  className: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.any,
};

ButtonLoading.defaultProps = {
  loading: {},
  className: '',
  onClick: () => {},
  children: null,
};

export default ButtonLoading;
