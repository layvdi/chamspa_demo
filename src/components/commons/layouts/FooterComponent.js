import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    position: "absolute",
    bottom: "10px",
    left: 0,
    right: 0,
    justifyContent: "center",
    zIndex: "1",
    color: theme.palette.common.logoColor
  }
}));

export default () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Typography>Copyright © JS-Studio</Typography>
    </div>
  );
};
