import React, { useState } from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import Badge from "@material-ui/core/Badge";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import NotificationsIcon from "@material-ui/icons/Notifications";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import AccountCircle from "@material-ui/icons/AccountCircle";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Link from "next/link";
import Router from "next/router";

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  toolbar: {
    paddingRight: 24 // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  Breadcrumbs: {
    flexGrow: 1
  },
  link: {
    color: theme.palette.common.link
  },
  textHeader: {
    color: "white"
  }
}));

export default function Dashboard(props) {
  const [openMenuUser, setOpenMenuUser] = useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);

  const classes = useStyles();

  const handleToggleOpenMenuUser = event => {
    setAnchorEl(event.currentTarget);
    setOpenMenuUser(!openMenuUser);
  };

  const getListBreadcrumb = () => {
    switch (Router.router.pathname) {
      case "/":
        return [{ label: "Bảng điều khiển", url: "/" }];
      case "/time_sheet":
        return [{ label: "Bảng điều khiển", url: "/" }, { label: "Chấm công", url: "/" }];
      case "/employee":
        return [{ label: "Bảng điều khiển", url: "/" }, { label: "Quản lí nhân viên", url: "/" }];
      case "/salary":
        return [{ label: "Bảng điều khiển", url: "/" }, { label: "Quản lí lương", url: "/" }];
      case "/department":
        return [{ label: "Bảng điều khiển", url: "/" }, { label: "Quản lí phòng ban", url: "/" }];
      case "/position":
        return [{ label: "Bảng điều khiển", url: "/" }, { label: "Chức vụ", url: "/" }];
      default:
        return [];
    }
  };

  const renderBreadCrumbs = () => {
    const listBreadcrumb = getListBreadcrumb();

    return (
      <Breadcrumbs aria-label="breadcrumb" className={classes.Breadcrumbs}>
        {listBreadcrumb.map((breadcrumbString, index) => {
          if (index == listBreadcrumb.length - 1) {
            return (
              <Typography className={classes.textHeader} key={index}>
                {breadcrumbString.label}
              </Typography>
            );
          }

          return (
            <Link href={breadcrumbString.url} key={index}>
              <a className={classes.link}>{breadcrumbString.label}</a>
            </Link>
          );
        })}
      </Breadcrumbs>
    );
  };

  return (
    <AppBar position="absolute" className={clsx(classes.appBar, props.openSlideBar && classes.appBarShift)}>
      <Toolbar className={classes.toolbar}>
        <IconButton edge="start" color="inherit" aria-label="open drawer" onClick={props.toggleSlideBarMenu}>
          {props.openSlideBar ? <ChevronLeftIcon /> : <MenuIcon />}
        </IconButton>

        {renderBreadCrumbs()}
        <IconButton color="inherit">
          <Badge badgeContent={4} color="error">
            <NotificationsIcon />
          </Badge>
        </IconButton>
        <IconButton color="inherit" onClick={handleToggleOpenMenuUser}>
          <AccountCircle />
          <Menu anchorEl={anchorEl} open={openMenuUser} onClose={handleToggleOpenMenuUser}>
            <MenuItem>Profile</MenuItem>
            <MenuItem onClick={props.handleLogout}>Logout</MenuItem>
          </Menu>
        </IconButton>
      </Toolbar>
    </AppBar>
  );
}
