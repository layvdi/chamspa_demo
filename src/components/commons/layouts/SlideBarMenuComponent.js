import React from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import DashboardIcon from "@material-ui/icons/Dashboard";
import PeopleIcon from "@material-ui/icons/People";
import BarChartIcon from "@material-ui/icons/BarChart";
import AssignmentTurnedIn from "@material-ui/icons/AssignmentTurnedIn";
import Settings from "@material-ui/icons/Settings";
import StarRate from "@material-ui/icons/StarRate";
import TableChart from "@material-ui/icons/TableChart";
import Router from "next/router";

import ListCollapseComponent from "../listCollapse/ListCollapseComponent";
import FooterComponent from "./FooterComponent";

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: theme.spacing(1),
    ...theme.mixins.toolbar,
    height: "64px",
    borderBottom: "1px #483d20 solid"
  },
  logo: {
    height: "100%"
  },
  drawerPaper: {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    }),
    backgroundImage: "url('static/images/sidebar.jpg')",
    boxShadow:
      "0 16px 38px -12px rgba(0, 0, 0, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)",
    "&::after": {
      content: "''",
      backgroundColor: "rgba(25, 25, 25, 0.9)",
      position: "absolute",
      right: "0",
      left: "0",
      bottom: "0",
      top: "0"
    }
  },
  drawerPaperContent: {
    zIndex: 1,
    color: theme.palette.common.white
  },
  Icon: {
    color: theme.palette.common.white
  },
  drawerPaperClose: {
    overflowX: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9)
    }
  },
  nested: {
    paddingLeft: theme.spacing(4)
  },
  active: {
    background: "#464646",
    color: "#f8b918"
  }
}));

export default function Dashboard(props) {
  const classes = useStyles();

  const getClassIfActive = path => {
    return Router.router.pathname == path ? classes.active : "";
  };

  return (
    <Drawer
      variant="permanent"
      classes={{
        paper: clsx(
          classes.drawerPaper,
          !props.openSlideBar && classes.drawerPaperClose
        )
      }}
      open={props.openSlideBar}
    >
      <div className={classes.drawerPaperContent}>
        <div className={classes.toolbarIcon}>
          <a href="/" className={classes.logo}>
            <img
              className={classes.logo}
              src="static/images/logo.png"
              alt="logo"
            />
          </a>
        </div>
        <Divider />
        <ListItem
          button
          className={getClassIfActive("/")}
          onClick={() => Router.push("/")}
        >
          <ListItemIcon>
            <DashboardIcon
              className={clsx(classes.Icon, getClassIfActive("/"))}
            />
          </ListItemIcon>
          <ListItemText primary="Bảng điều khiển" />
        </ListItem>
        <ListItem
          button
          className={getClassIfActive("/time_sheet")}
          onClick={() => Router.push("/time_sheet")}
        >
          <ListItemIcon>
            <AssignmentTurnedIn
              className={clsx(classes.Icon, getClassIfActive("/time_sheet"))}
            />
          </ListItemIcon>
          <ListItemText primary="Chấm công" />
        </ListItem>
        <ListItem
          button
          className={getClassIfActive("/employee")}
          onClick={() => Router.push("/employee")}
        >
          <ListItemIcon>
            <PeopleIcon
              className={clsx(classes.Icon, getClassIfActive("/employee"))}
            />
          </ListItemIcon>
          <ListItemText primary="Nhân viên" />
        </ListItem>
        <ListItem
          button
          className={getClassIfActive("/salary")}
          onClick={() => Router.push("/salary")}
        >
          <ListItemIcon>
            <BarChartIcon
              className={clsx(classes.Icon, getClassIfActive("/salary"))}
            />
          </ListItemIcon>
          <ListItemText primary="Lương" />
        </ListItem>
        <ListCollapseComponent
          text="Cơ cấu tổ chức"
          icon={<Settings className={classes.Icon} />}
        >
          <ListItem
            button
            className={clsx(classes.nested, getClassIfActive("/department"))}
            onClick={() => Router.push("/department")}
          >
            <ListItemIcon>
              <TableChart
                className={clsx(classes.Icon, getClassIfActive("/department"))}
              />
            </ListItemIcon>
            <ListItemText primary="Phòng ban" />
          </ListItem>
          <ListItem
            button
            className={clsx(classes.nested, getClassIfActive("/position"))}
            onClick={() => Router.push("/position")}
          >
            <ListItemIcon>
              <StarRate
                className={clsx(classes.Icon, getClassIfActive("/position"))}
              />
            </ListItemIcon>
            <ListItemText primary="Chức vụ" />
          </ListItem>
        </ListCollapseComponent>
        <Divider />
      </div>
      <FooterComponent />
    </Drawer>
  );
}
