import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  createStyles,
  withStyles,
  Paper,
  Table,
  TableRow,
  TableBody,
  TableHead,
  TableCell
} from "@material-ui/core";
import _ from "lodash";
import TablePagination from "@material-ui/core/TablePagination";

export default props => {
  const renderTableHead = () =>
    props.dataTable.columns.map((value, index) => (
      <TableCell key={index}>{value.header.label}</TableCell>
    ));

  const renderTableCellData = rowObject =>
    props.dataTable.columns.map((value, index) => {
      if (_.isFunction(value.cell)) {
        return <TableCell key={index}>{value.cell(rowObject)}</TableCell>;
      }
      return <TableCell key={index}>{rowObject[value.accessor_key]}</TableCell>;
    });

  const renderTableRowData = () =>
    props.dataTable.items.map((value, index) => (
      <TableRow key={index}>{renderTableCellData(value)}</TableRow>
    ));

  return (
    <>
      <Table>
        <TableHead>
          <TableRow>{renderTableHead()}</TableRow>
        </TableHead>
        <TableBody>{renderTableRowData()}</TableBody>
      </Table>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={100}
        rowsPerPage={10}
        page={1}
        backIconButtonProps={{
          "aria-label": "previous page"
        }}
        nextIconButtonProps={{
          "aria-label": "next page"
        }}
        onChangePage={() => {}}
        onChangeRowsPerPage={() => {}}
      />
    </>
  );
};
