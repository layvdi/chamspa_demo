import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

import snackbarContentComponent from './snackbarContentComponent';
import styles from './style';

const MySnackbarContentWrapper = withStyles(styles)(snackbarContentComponent);

class CustomizedSnackbars extends React.Component {
  render() {
    return (
      <Snackbar
        anchorOrigin={this.props.snackbar.anchorOrigin}
        open={this.props.snackbar.isOpen}
        autoHideDuration={this.props.snackbar.autoHideDuration}
        onClose={this.props.handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={this.props.handleCloseSnackbar}
          variant={this.props.snackbar.variant}
          message={this.props.snackbar.message}
        />
      </Snackbar>
    );
  }
}

CustomizedSnackbars.propTypes = {
  snackbar: PropTypes.object.isRequired,
  handleCloseSnackbar: PropTypes.func.isRequired,
};

export default CustomizedSnackbars;
