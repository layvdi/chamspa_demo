import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import makeStyles from "@material-ui/styles/makeStyles";

const useStyles = makeStyles(theme => ({
  buttonDel: {
    backgroundColor: "red",
    marginRight: "5px"
  }
}));

// openPopupDel, setOpenPopupDel
export default function AlertDialog(props) {
  const classes = useStyles();
  return (
    <Dialog
      open={props.openPopupDel}
      onClose={() => props.setOpenPopupDel(false)}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Xác nhận</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Bạn có muốn xóa <strong>{props.title}</strong>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          variant="contained"
          onClick={() => {
            props.deleteDepartment();
            props.setOpenPopupDel(false);
          }}
          color="primary"
          className={classes.buttonDel}
        >
          Xóa
        </Button>
        <Button variant="contained" onClick={() => props.setOpenPopupDel(false)} autoFocus>
          Hủy
        </Button>
      </DialogActions>
    </Dialog>
  );
}
