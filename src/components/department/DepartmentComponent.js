import { useEffect, useState, useRef } from "react";
import { Paper } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import Tree from "react-d3-tree";
import _ from "lodash";

import DepartmentDetailComponent from "./DepartmentDetailComponent";
import "../../static/styles/customStyle.css";

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: "#464646",
    display: "flex",
    justifyContent: "center"
  },
  content: {
    width: "100%",
    minHeight: "85vh"
  }
}));

const convertObjectToArray = (object, keyRef) => {
  const newArray = [];
  for (const key in object) {
    newArray.push({
      [keyRef]: key,
      ...object[key]
    });
  }

  return newArray;
};

const convertAllPropertiesOfObjectToArray = (object, keyToArray = "children", keyRef = "ref") => {
  for (const key in object) {
    if (key === keyToArray) {
      const arrayNew = convertObjectToArray(object[key], keyRef);

      object[key] = arrayNew;
    }

    if (typeof object[key] === "object") {
      convertAllPropertiesOfObjectToArray(object[key]);
    }
  }

  return object;
};

export default props => {
  const treeContainer = useRef(null);
  const [translate, setTranslate] = useState({ x: 0, y: 0 });
  const [openDrawer, toggleOpenDrawer] = useState(false);
  const [departmentSelected, setDepartmentSelected] = useState({});
  const [dataTree, setDataTree] = useState([
    {
      name: "Công ty",
      ref: "department"
    }
  ]);

  const convertDataToTree = () => {
    try {
      const departmentChildren = { ...props.departmentReducer.list };

      const dataTreeTemp = [
        {
          name: "Công ty",
          ref: "department",
          children: departmentChildren
        }
      ];
      convertAllPropertiesOfObjectToArray(dataTreeTemp, "children", "ref");

      setDataTree(dataTreeTemp);
    } catch (ex) {}
  };

  useEffect(() => {
    if (!treeContainer) {
      return;
    }

    const dimensions = treeContainer.current.getBoundingClientRect();

    if (dimensions) {
      setTranslate({
        x: dimensions.width / 2.5,
        y: dimensions.height / 2
      });
    }

    convertDataToTree();
  }, [props.departmentReducer.list]);

  const classes = useStyles();

  return (
    <Paper className={classes.root}>
      <div className={classes.content} ref={treeContainer}>
        <Tree
          data={dataTree}
          styles={{ width: "100%", height: "100%" }}
          onClick={node => {
            toggleOpenDrawer(false);

            setTimeout(() => {
              toggleOpenDrawer(true);
            }, 100);
            setDepartmentSelected(node);
          }}
          collapsible={false}
          translate={translate}
        />
      </div>
      <DepartmentDetailComponent
        editDepartment={props.editDepartment}
        departmentReducer={props.departmentReducer}
        openDrawer={openDrawer}
        deleteDepartment={props.deleteDepartment}
        addDepartment={props.addDepartment}
        toggleOpenDrawer={toggleOpenDrawer}
        departmentSelected={departmentSelected}
      />
    </Paper>
  );
};
