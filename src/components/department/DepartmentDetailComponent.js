import React from "react";
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Close from "@material-ui/icons/Close";
import Save from "@material-ui/icons/Save";
import Delete from "@material-ui/icons/Delete";
import { TextField, Button } from "@material-ui/core";

import AddDepartmentComponent from "./AddDepartmentComponent";
import DeleteDepartmentComponent from "./DeleteDepartmentComponent";

const drawerWidth = "25vw";

const useStyles = makeStyles(theme => ({
  drawer: {
    width: drawerWidth,
    position: "absolute",
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth
  },
  content: {
    marginTop: theme.mixins.toolbar.minHeight,
    padding: theme.spacing(2),
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    position: "relative"
  },
  iconClose: {
    color: "red"
  },
  iconCloseWrapper: {
    position: "absolute",
    top: 10,
    right: 10
  },
  footer: {
    marginTop: "50px"
  },
  button: {
    backgroundColor: "#26c6da",
    marginRight: "5px"
  },
  add: {
    marginTop: "20px",
    alignSelf: "flex-end",
    textDecoration: "none"
  },
  buttonDel: {
    backgroundColor: "red",
    marginRight: "5px"
  }
}));

const getPathFromRef = object => {
  let str = object.ref;
  if (object.children && object.ref != "department") {
    str += "/children";
  }

  if (object.parent) {
    str = `${str} ${getPathFromRef(object.parent)}`;
  }

  return str;
};

const getRef = department => {
  let pathList = getPathFromRef(department);
  pathList = pathList
    .split(" ")
    .reverse()
    .join("/");
  return pathList;
};

export default function PersistentDrawerLeft(props) {
  const [openPopup, setOpenPopup] = React.useState(false);
  const [openPopupDel, setOpenPopupDel] = React.useState(false);
  const [department, setDepartment] = React.useState({});
  const classes = useStyles();

  React.useEffect(() => {
    setDepartment(props.departmentSelected);
  }, [props.departmentSelected]);

  const handleUpdateData = () => {
    const ref = getRef(department);
    props.editDepartment({
      ref,
      name: department.name,
      phone: department.phone
    });
  };

  const handleDeleteDepartment = () => {
    const ref = getRef(department);
    props.deleteDepartment({ ref });
    props.toggleOpenDrawer(false);
  };

  const handleAddDepartment = data => {
    const ref = getRef(department);
    props.addDepartment({ ref, ...data });
    props.toggleOpenDrawer(false);
  };

  return (
    <>
      <AddDepartmentComponent
        addDepartment={handleAddDepartment}
        {...{ openPopup, setOpenPopup }}
      ></AddDepartmentComponent>
      <DeleteDepartmentComponent
        deleteDepartment={handleDeleteDepartment}
        title={props.departmentSelected.name}
        {...{ openPopupDel, setOpenPopupDel }}
      ></DeleteDepartmentComponent>
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="right"
        open={props.openDrawer}
        classes={{
          paper: classes.drawerPaper
        }}
      >
        <div className={classes.content}>
          <IconButton className={classes.iconCloseWrapper} onClick={() => props.toggleOpenDrawer(!props.openDrawer)}>
            <Close className={classes.iconClose}></Close>
          </IconButton>
          <h2>Thông Tin Phòng Ban</h2>
          <TextField
            id="standard-dense"
            margin="dense"
            InputLabelProps={{
              shrink: true
            }}
            onChange={e => setDepartment({ ...department, name: e.target.value })}
            value={department.name}
            fullWidth
            label="Tên phòng ban"
            margin="normal"
          />
          <TextField
            InputLabelProps={{
              shrink: true
            }}
            id="standard-name"
            onChange={e => setDepartment({ ...department, phone: e.target.value })}
            value={department.phone}
            fullWidth
            label="Số điện thoại"
            margin="normal"
          />
          <a href="#" onClick={() => setOpenPopup(true)} className={classes.add}>
            Thêm phòng ban
          </a>
          {props.departmentSelected.ref == "department" ? null : (
            <div className={classes.footer}>
              <Button
                variant="contained"
                color="primary"
                onClick={() => {
                  setOpenPopupDel(true);
                }}
                className={classes.buttonDel}
              >
                Xóa
                <Delete fontSize="small"></Delete>
              </Button>
              <Button variant="contained" color="primary" className={classes.button} onClick={handleUpdateData}>
                cập nhật
                <Save fontSize="small"></Save>
              </Button>
            </div>
          )}
        </div>
      </Drawer>
    </>
  );
}
