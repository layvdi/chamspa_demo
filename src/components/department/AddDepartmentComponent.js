import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";
import { TextField } from "@material-ui/core";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function AlertDialogSlide(props) {
  const [data, setData] = React.useState({ name: "", phone: "" });
  return (
    <Dialog
      open={props.openPopup}
      TransitionComponent={Transition}
      keepMounted
      aria-labelledby="alert-dialog-slide-title"
      aria-describedby="alert-dialog-slide-description"
    >
      <DialogTitle id="alert-dialog-slide-title">Thêm mới phòng ban</DialogTitle>
      <DialogContent>
        <TextField
          id="standard-name"
          value={data.name}
          onChange={e => setData({ ...data, name: e.target.value })}
          fullWidth
          label="Tên phòng ban"
          margin="normal"
        />
        <TextField
          id="standard-name"
          value={data.phone}
          onChange={e => setData({ ...data, phone: e.target.value })}
          fullWidth
          label="Số điện thoại"
          margin="normal"
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={() => props.setOpenPopup(!props.openPopup)} color="primary">
          Hủy
        </Button>
        <Button
          onClick={() => {
            props.addDepartment(data);
            props.setOpenPopup(!props.openPopup);
            setData({ name: "", phone: "" });
          }}
          color="primary"
        >
          Thêm
        </Button>
      </DialogActions>
    </Dialog>
  );
}
