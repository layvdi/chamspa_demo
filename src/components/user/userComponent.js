import React, { Component } from 'react';
import propTypes from 'prop-types';
import { Card, CardActions, CardHeader, Button, CardContent, TextField } from '@material-ui/core'
import SaveIcon from '@material-ui/icons/Save';

class UserComponent extends Component {
    render() {
      return (
        <div>
          <Card elevation={1}>
            <CardHeader title="User Information" />
            <CardContent>
              <TextField
                id="standard-dense"
                label="First Name"
                value={this.props.user.firstName}
                margin="dense"
                fullWidth
              />
              <TextField
                id="standard-dense"
                label="Last Name"
                value={this.props.user.lastName}
                margin="dense"
                fullWidth
              />
              <TextField
                id="standard-dense"
                label="Eamil"
                value={this.props.user.email}
                margin="dense"
                fullWidth
              />
            </CardContent>
            <CardActions>
              <Button variant="contained" color="primary">
                <SaveIcon />
                Save
              </Button>
            </CardActions>
          </Card>
        </div>
      )
    }
}

UserComponent.propTypes = {
  user: propTypes.object.isRequired,
};

export default UserComponent;
