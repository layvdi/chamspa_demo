import React, { Component } from "react";
import propTypes from "prop-types";
import Avatar from "@material-ui/core/Avatar";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import { FormHelperText, createStyles } from "@material-ui/core";
import withStyles from "@material-ui/core/styles/withStyles";

import * as validator from "validator";
import * as _ from "lodash";

import ButtonLoading from "../commons/buttonLoading";

const styles = theme =>
  createStyles({
    main: {
      width: "auto",
      display: "block", // Fix IE 11 issue.
      marginLeft: theme.spacing.unit * 3,
      marginRight: theme.spacing.unit * 3,
      [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
        width: 400,
        marginLeft: "auto",
        marginRight: "auto"
      }
    },
    paper: {
      marginTop: theme.spacing.unit * 8,
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
        .spacing.unit * 3}px`
    },
    avatar: {
      margin: theme.spacing.unit,
      backgroundColor: theme.palette.secondary.main
    },
    form: {
      width: "100%", // Fix IE 11 issue.
      marginTop: theme.spacing.unit
    },
    submit: {
      marginTop: theme.spacing.unit * 3
    }
  });

class LoginComponent extends Component {
  state = {
    loginData: {
      email: {
        value: "",
        error: ""
      },
      password: {
        value: "",
        error: ""
      }
    }
  };

  handleChangeLoginData = e => {
    const { name, value } = e.target;

    const { loginData } = this.state;

    loginData[name] = {
      value
    };

    this.setState({
      loginData
    });
  };

  validateLoginData = () => {
    const { loginData } = this.state;
    let isValidData = true;

    if (validator.isEmpty(this.state.loginData.email.value)) {
      loginData.email.error = "Email is required";
      isValidData = false;
    }

    if (validator.isEmpty(this.state.loginData.password.value)) {
      loginData.password.error = "Password is required";
      isValidData = false;
    }

    this.setState({
      loginData
    });

    return isValidData;
  };

  handleLogin = () => {
    if (!this.validateLoginData()) {
      return;
    }

    this.props.handleLogin({
      email: this.state.loginData.email.value,
      password: this.state.loginData.password.value
    });
  };

  render() {
    const { classes } = this.props;
    return (
      <main className={classes.main}>
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <p>admin@gmail.com/123456789</p>
          <form className={classes.form}>
            <FormControl
              margin="normal"
              fullWidth
              error={!_.isEmpty(this.state.loginData.email.error)}
            >
              <InputLabel htmlFor="email">Email</InputLabel>
              <Input
                name="email"
                autoFocus
                value={this.state.loginData.email.value}
                onChange={this.handleChangeLoginData}
              />
              <FormHelperText id="component-error-text">
                {this.state.loginData.email.error}
              </FormHelperText>
            </FormControl>
            <FormControl
              margin="normal"
              fullWidth
              error={!_.isEmpty(this.state.loginData.password.error)}
            >
              <InputLabel htmlFor="password">Password</InputLabel>
              <Input
                name="password"
                type="password"
                value={this.state.loginData.password.value}
                onChange={this.handleChangeLoginData}
              />
              <FormHelperText id="component-error-text">
                {this.state.loginData.password.error}
              </FormHelperText>
            </FormControl>
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <ButtonLoading
              type="button"
              fullWidth
              variant="contained"
              color="primary"
              onClick={this.handleLogin}
              className={classes.submit}
              messageLoading="Sign in..."
              isLoading={this.props.appLoading.isLoading}
            >
              Sign in
            </ButtonLoading>
          </form>
        </Paper>
      </main>
    );
  }
}

LoginComponent.propTypes = {
  handleLogin: propTypes.func.isRequired,
  classes: propTypes.object.isRequired,
  appLoading: propTypes.object.isRequired
};

export default withStyles(styles)(LoginComponent);
