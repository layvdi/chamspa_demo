import React from "react";
import PropTypes from "prop-types";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import Visibility from "@material-ui/icons/Visibility";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Print from "@material-ui/icons/Print";

import { Typography } from "@material-ui/core";
import DataTableComponent from "../commons/dataTable/DataTableComponent";

const useStyles = makeStyles(theme => ({
  root: {
    color: theme.palette.text.secondary,
    padding: theme.spacing(1),
    width: "100%"
  },
  headerTable: {
    display: "flex",
    justifyContent: "space-between"
  }
}));

const items = [
  {
    id: "1",
    code: "1",
    first_name: "1",
    last_name: "1",
    department: "",
    position: "",
    time_sheet: []
  }
];

const columns = [
  {
    header: {
      label: "Tên nhân viên"
    },
    accessor_key: "first_name"
  },
  {
    header: {
      label: "Phòng ban"
    },
    accessor_key: "first_name"
  },
  {
    header: {
      label: "Chức vụ"
    },
    accessor_key: "first_name"
  },
  {
    header: {
      label: "Mon - 1/8"
    },
    accessor_key: "first_name"
  },
  {
    header: {
      label: "Tue - 2/8"
    },
    accessor_key: "first_name"
  },
  {
    header: {
      label: "Web - 2/8"
    },
    accessor_key: "first_name"
  },
  {
    header: {
      label: "Thur - 2/8"
    },
    accessor_key: "first_name"
  },
  {
    header: {
      label: "Fir - 2/8"
    },
    accessor_key: "first_name"
  },
  {
    header: {
      label: "Sat - 2/8"
    },
    accessor_key: "first_name"
  },
  {
    header: {
      label: "Sun - 2/8"
    },
    accessor_key: "first_name"
  },
  {
    header: {
      label: "Total"
    },
    accessor_key: "first_name"
  }
];

export default function CustomPaginationActionsTable() {
  const classes = useStyles();
  return (
    <Paper className={classes.root}>
      <div className={classes.headerTable}>
        <Typography>Tháng</Typography>
        <div>
          <Button>
            <Print />
            <Typography>In</Typography>
          </Button>
          <Button>
            <SaveAlt />
            <Typography>Xuất excel</Typography>
          </Button>
          <Button>
            <Visibility />
            <Typography>Tháng</Typography>
          </Button>
        </div>
      </div>
      <DataTableComponent dataTable={{ columns, items }} showPagination />
    </Paper>
  );
}
