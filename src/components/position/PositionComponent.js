import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Fab } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import Delete from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";

import DetailPositionComponent from "./DetailPositionComponent";
import AddPositionComponent from "./AddPositionComponent";
import DeleteDepartmentComponent from "./DeleteDepartmentComponent";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    position: "relative"
  },
  table: {
    minWidth: 650
  },
  btnAdd: {
    position: "absolute",
    bottom: -70,
    right: 10,
    backgroundColor: "#198d9c"
  },
  btnDel: {
    color: "red",
    padding: "none"
  }
}));

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white
  },
  body: {
    fontSize: 14
  }
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.background.default
    }
  }
}))(TableRow);

export default function SimpleTable(props) {
  const [openPopup, setOpenPopup] = React.useState(false);
  const [openPopupAdd, setOpenPopupAdd] = React.useState(false);
  const [openPopupDel, setOpenPopupDel] = React.useState(false);
  const [positionSelected, setPositionSelected] = React.useState({});

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <DetailPositionComponent
        editPosition={props.editPosition}
        positionSelected={positionSelected}
        {...{ openPopup, setOpenPopup }}
      ></DetailPositionComponent>
      <AddPositionComponent
        addPosition={props.addPosition}
        {...{ openPopupAdd, setOpenPopupAdd }}
      ></AddPositionComponent>
      <DeleteDepartmentComponent
        delPosition={props.delPosition}
        positionSelected={positionSelected}
        {...{ openPopupDel, setOpenPopupDel }}
      ></DeleteDepartmentComponent>
      <Fab
        size="medium"
        color="secondary"
        onClick={() => setOpenPopupAdd(true)}
        aria-label="add"
        className={classes.btnAdd}
      >
        <AddIcon />
      </Fab>
      <Paper>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <StyledTableCell>STT</StyledTableCell>
              <StyledTableCell>Tên chức vụ</StyledTableCell>
              <StyledTableCell>Ghi chú</StyledTableCell>
              <StyledTableCell>Ngày tạo</StyledTableCell>
              <StyledTableCell></StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {props.positions.map((row, index) => (
              <StyledTableRow key={row.name}>
                <StyledTableCell component="th" scope="row">
                  {index + 1}
                </StyledTableCell>
                <StyledTableCell component="th" scope="row">
                  <a
                    href="#"
                    onClick={() => {
                      setOpenPopup(true);
                      setPositionSelected(row);
                    }}
                  >
                    {row.name}
                  </a>
                </StyledTableCell>
                <StyledTableCell>{row.description}</StyledTableCell>
                <StyledTableCell>{row.create_at}</StyledTableCell>
                <StyledTableCell>
                  <IconButton
                    onClick={() => {
                      setOpenPopupDel(true);
                      setPositionSelected(row);
                    }}
                    className={classes.btnDel}
                  >
                    <Delete></Delete>
                  </IconButton>
                </StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    </div>
  );
}
