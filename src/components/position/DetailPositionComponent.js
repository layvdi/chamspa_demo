import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";
import { TextField } from "@material-ui/core";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function AlertDialogSlide(props) {
  const [data, setData] = React.useState({ name: "", description: "" });

  React.useEffect(() => {
    setData(props.positionSelected);
  }, [props.positionSelected]);

  return (
    <Dialog
      open={props.openPopup}
      TransitionComponent={Transition}
      keepMounted
      aria-labelledby="alert-dialog-slide-title"
      aria-describedby="alert-dialog-slide-description"
    >
      <DialogTitle id="alert-dialog-slide-title">Thông tin chức vụ</DialogTitle>
      <DialogContent>
        <TextField
          id="standard-name"
          value={data.name}
          onChange={e => setData({ ...data, name: e.target.value })}
          fullWidth
          label="Tên chức vụ"
          margin="normal"
          InputLabelProps={{
            shrink: true
          }}
        />
        <TextField
          id="standard-name"
          value={data.description || "..."}
          onChange={e => setData({ ...data, description: e.target.value })}
          fullWidth
          label="Mô tả"
          margin="normal"
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={() => props.setOpenPopup(!props.openPopup)} color="primary">
          Hủy
        </Button>
        <Button
          onClick={() => {
            props.editPosition({ ...data, id: props.positionSelected.id });
            props.setOpenPopup(!props.openPopup);
            setData({ name: "", description: "" });
          }}
          color="primary"
        >
          lưu
        </Button>
      </DialogActions>
    </Dialog>
  );
}
