import { createMuiTheme } from "@material-ui/core/styles";
import { red } from "@material-ui/core/colors";

const hightLightColor = "#D11521";
const grayColor = "#E4E4E4";

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#000c17"
    },
    secondary: {
      main: "#F1F6F6"
    },
    error: {
      main: red.A400
    },
    background: {
      default: "#F1F6F6"
    },
    common: {
      hightLight: {
        main: hightLightColor
      },
      textColor: {
        main: "#13BEBB",
        dark: "#0A2623"
      },
      gray: grayColor,
      logoColor: "#f8b918",
      link: "rgb(96, 96, 255)"
    },
    text: {
      primary: "#000",
      secondary: "#000"
    }
  },
  typography: {
    fontFamily: ["Roboto"],
    fontColor: "red"
  }
});

export default theme;
