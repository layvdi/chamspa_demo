export const convertObjectToArray = (object, keyRef, justKey) => {
  const newArray = [];

  for (const key in object) {
    const newObject = {
      ...object[key]
    };
    if (keyRef) {
      newObject[keyRef] = key;
    }
    if (justKey) {
      newArray.push(key);
    } else {
      newArray.push(newObject);
    }
  }

  return newArray;
};

export const convertAllPropertiesOfObjectToArray = (object, keyToArray = [], keyRef, justKey = true) => {
  for (const key in object) {
    if (keyToArray.includes(key)) {
      const arrayNew = this.convertObjectToArray(object[key], keyRef, justKey);
      object[key] = arrayNew;
    }

    if (typeof object[key] === "object") {
      this.convertAllPropertiesOfObjectToArray(object[key], keyToArray, keyRef, justKey);
    }
  }

  return object;
};
