import * as firebase from "firebase/app";

import firebaseConfig from "../../../configs/firebaseConfig";

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export default firebase;
