import firebaseService from "./firebaseService";

require("firebase/database");

export const database = firebaseService.database();

export const getList = async () => {
  try {
    const snapshot = await database.ref("department").once("value");
    return snapshot.val();
  } catch (ex) {
    throw ex;
  }
};
export const addDepartment = async (ref, name, phone) => {
  const pathList = ref.split("/");

  const child = pathList[pathList.length - 1];

  if (ref != "department" && child != "children") {
    ref += "/children";
  }
  const postListRef = database.ref(ref);
  const newPostRef = postListRef.push();
  await newPostRef.set({
    name,
    phone
  });
};
export const updateDepartment = async (ref, name, phone) => {
  const pathList = ref.split("/");

  const child = pathList[pathList.length - 1];
  if (child == "children") {
    pathList.splice(pathList.length - 1, 1);
    ref = pathList.join("/");
  }

  await database.ref(ref).update({
    name,
    phone
  });
};
export const delDepartment = async ref => {
  const pathList = ref.split("/");

  const child = pathList[pathList.length - 1];
  if (child == "children") {
    pathList.splice(pathList.length - 1, 1);
    ref = pathList.join("/");
  }
  await database.ref(ref).remove();
};
