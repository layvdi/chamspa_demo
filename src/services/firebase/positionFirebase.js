import moment from "moment";

import firebaseService from "./firebaseService";

require("firebase/database");

export const database = firebaseService.database();

export const getList = async () => {
  try {
    const snapshot = await database.ref("position").once("value");
    return snapshot.val();
  } catch (ex) {
    throw ex;
  }
};

export const addPosition = async (name, description) => {
  const postListRef = database.ref("position");
  const newPostRef = postListRef.push();
  await newPostRef.set({
    name,
    description,
    create_at: moment().format("DD-MM-YYYY")
  });
};

export const updatePosition = async (id, name, description) => {
  await database.ref(`position/${id}`).update({
    name,
    description
  });
};

export const delPosition = async id => {
  await database.ref(`position/${id}`).remove();
};
