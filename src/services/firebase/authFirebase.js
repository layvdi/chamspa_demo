import firebaseService from "./firebaseService";
import "firebase/auth";
import "firebase/firestore";

export const login = async ({ email, password }) => {
  try {
    await firebaseService.auth().signInWithEmailAndPassword(email, password);
    const currentUser = firebaseService.auth().currentUser;
    const emailRes = await currentUser.email;
    const tokenRes = await currentUser.getIdToken();
    return { email: emailRes, token: tokenRes };
  } catch (ex) {
    throw ex;
  }
};
