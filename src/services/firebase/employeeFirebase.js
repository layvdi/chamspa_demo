import firebaseService from "./firebaseService";

require("firebase/database");

export const database = firebaseService.database();

export const getList = async () => {
  try {
    const snapshot = await database.ref("employee").once("value");
    return snapshot.val();
  } catch (ex) {
    throw ex;
  }
};
export const addEmployee = async (ref ,name, birthday , postision_id , department_id) => {
  const postListRef = database.ref(ref);
  const newPostRef = postListRef.push();
  await newPostRef.set({
    name,
    birthday,
    postision_id,
    department_id
  })
}
export const updateEmployee = async (ref ,name, birthday , postision_id , department_id) => {
  await database.ref(ref).update({
    name,
    birthday,
    postision_id,
    department_id
  })
}
export const delEmployee = async(ref) => {
  await database.ref(ref).remove();
}