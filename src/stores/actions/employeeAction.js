import * as actionTypes from "../constants";

export const getList = () => ({
  type: actionTypes.EMPLOYEE_GET_LIST
});

export const getListFail = () => ({
  type: actionTypes.EMPLOYEE_GET_LIST_FAIL
});
export const getListSuccess = data => ({
  type: actionTypes.EMPLOYEE_GET_LIST_SUCCESS,
  data
});
export const postAdd = (data) => ({
  type: actionTypes.EMPLOYEE_ADD,
  data
});

export const putEdit = (data) => ({
  type: actionTypes.EMPLOYEE_EDIT,
  data
});
export const del = (data) => ({
  type: actionTypes.EMPLOYEE_DEL,
  data
});

