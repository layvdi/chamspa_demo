import * as actionTypes from '../constants';

export function setAppLoading(data) {
  return { data, type: actionTypes.APP_LOADING };
}

export function setAppCancelLoading() {
  return { type: actionTypes.APP_CANCEL_LOADING };
}

export const openSnackbar = data => ({
  type: actionTypes.OPEN_SNACKBAR,
  data,
});

export const closeSnackbar = () => ({
  type: actionTypes.CLOSE_SNACKBAR,
});

export const throwError = (data) => ({
  type: actionTypes.THROW_ERROR,
  data,
});