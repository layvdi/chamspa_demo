import * as actionTypes from "../constants";

export const getList = () => ({
  type: actionTypes.DEPARTMENT_GET_LIST
});

export const getListFail = () => ({
  type: actionTypes.DEPARTMENT_GET_LIST_FAIL
});
export const getListSuccess = data => ({
  type: actionTypes.DEPARTMENT_GET_LIST_SUCCESS,
  data
});
export const postAdd = (data) => ({
  type: actionTypes.DEPARTMENT_ADD,
  data
});

export const putEdit = (data) => ({
  type: actionTypes.DEPARTMENT_EDIT,
  data
});
export const del = (data) => ({
  type: actionTypes.DEPARTMENT_DEL,
  data
});

