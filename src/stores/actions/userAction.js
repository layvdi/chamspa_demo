import * as actionTypes from '../constants';

export function getListUserSuccess(data) {
  return { data, type: actionTypes.USER_GET_LIST_SUCCESS };
}

export function doGetListUser(data) {
  return { data, type: actionTypes.USER_GET_LIST };
}

export function getUserSuccess(data) {
  return { data, type: actionTypes.USER_GET_SUCCESS };
}

export function doGetUser(data) {
  return { data, type: actionTypes.USER_GET };
}

