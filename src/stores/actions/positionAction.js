import * as actionTypes from "../constants";

export const getList = () => ({
  type: actionTypes.POSITION_GET_LIST
});

export const getListFail = () => ({
  type: actionTypes.POSITION_GET_LIST_FAIL
});
export const getListSuccess = data => ({
  type: actionTypes.POSITION_GET_LIST_SUCCESS,
  data
});
export const postAdd = data => ({
  type: actionTypes.POSITION_ADD,
  data
});

export const putEdit = data => ({
  type: actionTypes.POSITION_EDIT,
  data
});
export const del = data => ({
  type: actionTypes.POSITION_DEL,
  data
});
