import { combineReducers } from 'redux';

import authReducer from './authReducer';
import appReducer from './appReducer';
import userReducer from './userReducer';
import departmentReducer from './departmentReducer';
import positionReducer from './positionReducer';
import employeeReducer from './employeeReducer';

export default combineReducers({
  authReducer,
  appReducer,
  userReducer,
  departmentReducer,
  positionReducer,
  employeeReducer
});
