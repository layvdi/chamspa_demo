import * as actionTypes from '../constants';

const initialState = {
  appLoading: {
    isLoading: false,
    message: '',
  },

  appSnackbar: {
    isOpen: false,
    anchorOrigin: {
      vertical: 'bottom',
      horizontal: 'center',
    },
    autoHideDuration: 3000,
    variant: 'success',
    message: 'Success message!',
  },
};

const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.APP_LOADING:
      return {
        ...state,
        appLoading: {
          isLoading: true,
          ...action.data,
        },
      };
    case actionTypes.APP_CANCEL_LOADING:
      return {
        ...state,
        appLoading: initialState.appLoading,
      };
    case actionTypes.OPEN_SNACKBAR:
      return {
        ...state,
        appSnackbar: {
          ...state.appSnackbar,
          ...action.data,
          isOpen: true,
        },
      };
    case actionTypes.CLOSE_SNACKBAR:
      return {
        ...state,
        appSnackbar: {
          ...state.appSnackbar,
          isOpen: false,
        },
      };
    default:
      return state;
  }
};

export default appReducer;
