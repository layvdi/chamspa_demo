import * as actionTypes from '../constants';

const initialState = {};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTH_DO_LOGIN_SUCCESS:
      return {
        ...state,
        token: action.data.token,
        user: action.data.user,
      };

    case actionTypes.AUTH_DO_LOG_OUT:
      return initialState;
    default:
      return state;
  }
};

export default authReducer;
