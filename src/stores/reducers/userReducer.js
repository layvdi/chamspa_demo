import * as actionTypes from '../constants';

const initialState = {
  listUser: {
    items: [],
    pagination: {}
  },
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.USER_GET_LIST_SUCCESS:
      return {
        ...state,
        listUser: action.data,
      };
    case actionTypes.USER_GET_SUCCESS:
      return {
        ...state,
        User: action.data,
      };
    default:
      return state;
  }
};

export default userReducer;
