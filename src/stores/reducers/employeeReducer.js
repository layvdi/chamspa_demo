import * as actionTypes from '../constants';

const initialState = {
  list: [],
  status: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.EMPLOYEE_GET_LIST:
      return {
        ...state,
        status: 'loading',
      };
    case actionTypes.EMPLOYEE_GET_LIST_SUCCESS:
      return {
        ...state,
        list: action.data,
        status: 'success',
      };
    case actionTypes.EMPLOYEE_GET_LIST_FAIL:
      return {
        ...state,
        status: 'fail',
      };
    case actionTypes.EMPLOYEE_ADD:
      return {
        ...state,
        status: 'adding',
      };
    case actionTypes.EMPLOYEE_EDIT:
      return {
        ...state,
        status: 'updating',
      };
    case actionTypes.EMPLOYEE_DEL:
      return {
        ...state,
        status: 'deleting',
      };
    default:
      return state;
  }
};
