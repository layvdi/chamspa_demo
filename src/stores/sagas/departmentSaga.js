import { takeEvery, call, put, race, delay } from "redux-saga/effects";
import * as HttpStatus from "http-status-codes";
import _ from "lodash";

import * as departmentAction from "../actions/departmentAction";
import * as actions from "../actions";
import * as actionTypes from "../constants";
import { getList,addDepartment ,updateDepartment,delDepartment} from "../../services/firebase/departmentFirebase";

function* doGetList() {
  try {
    const { posts, timeout } = yield race({
      posts: call(getList),
      timeout: delay(10000)
    });
    if (timeout) {
      const error = new Error();
      error.status = HttpStatus.REQUEST_TIMEOUT;
      throw error;
    }

    yield put(departmentAction.getListSuccess(posts));
  } catch (error) {
    yield put(departmentAction.getListFail());
    yield put(actions.throwError(error));
  }
}
function* doAdd(data) {
  try {
    const department = data.data;
    const { timeout } = yield race({
      posts: call(addDepartment, department.ref,department.name,department.phone),
      timeout: delay(10000)
    });
    if (timeout) {
      const error = new Error();
      error.status = HttpStatus.REQUEST_TIMEOUT;
      throw error;
    }
    yield put(
      actions.openSnackbar({
        variant: "success",
        message: "Add successful",
        autoHideDuration: 2000
      })
    );
  } catch (error) {
    yield put(
      actions.openSnackbar({
        variant: "error",
        message: "Add Failed",
        autoHideDuration: 4000
      })
    );
    yield put(actions.throwError(error));
  }
}
function* doUpdate(data) {
  try {
    const department = data.data;
    const { timeout } = yield race({
      posts: call(updateDepartment, department.ref,department.name,department.phone),
      timeout: delay(10000)
    });
    if (timeout) {
      const error = new Error();
      error.status = HttpStatus.REQUEST_TIMEOUT;
      throw error;
    }
    yield put(
      actions.openSnackbar({
        variant: "success",
        message: "Update successful",
        autoHideDuration: 2000
      })
    );
  } catch (error) {
    yield put(
      actions.openSnackbar({
        variant: "error",
        message: "Update Failed",
        autoHideDuration: 4000
      })
    );
    yield put(actions.throwError(error));
  }
}
function* doDelete(data) {
  try {
    const department = data.data;
    const { timeout } = yield race({
      posts: call(delDepartment, department.ref),
      timeout: delay(10000)
    });
    if (timeout) {
      const error = new Error();
      error.status = HttpStatus.REQUEST_TIMEOUT;
      throw error;
    }
    yield put(
      actions.openSnackbar({
        variant: "success",
        message: "Delete successful",
        autoHideDuration: 2000
      })
    );
  } catch (error) {
    yield put(
      actions.openSnackbar({
        variant: "error",
        message: "Delete Failed",
        autoHideDuration: 4000
      })
    );
    yield put(actions.throwError(error));
  }
}
export default function* watchDepartment() {
  yield takeEvery(actionTypes.DEPARTMENT_GET_LIST, doGetList);
  yield takeEvery(actionTypes.DEPARTMENT_ADD, doAdd);
  yield takeEvery(actionTypes.DEPARTMENT_EDIT, doUpdate);
  yield takeEvery(actionTypes.DEPARTMENT_DEL, doDelete);
}
