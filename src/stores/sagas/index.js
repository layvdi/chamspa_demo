import { all } from "redux-saga/effects";

import watchAuth from "./authSaga";
import watchUser from "./userSaga";
import watchApp from "./appSaga";
import watchDepartment from "./departmentSaga";
import watchPosition from "./positionSaga";
import watchEmployee from "./employeeSaga";

export default function* rootSaga() {
  yield all([watchAuth(), watchUser(), watchApp(), watchDepartment(), watchPosition(), watchEmployee()]);
}
