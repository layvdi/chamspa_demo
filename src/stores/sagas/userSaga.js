// import * as _ from 'lodash';
// import {
//   takeEvery, call, put, race, delay, select
// } from 'redux-saga/effects';

// import * as actions from '../actions';
// import * as actionTypes from '../constants';
// import * as Api from '../../services/api';

// function* doGetListUser(action) {
//   const { data } = action;

//   yield put(
//     actions.setAppLoading({
//       isLoading: true,
//       message: 'Login...',
//     }),
//   );

//   yield

//   try {
//     const token = yield select((state) => state.authReducer.token)

//     const resultResponse = yield call(Api.getListUser, data, token);

//     yield put(actions.setAppCancelLoading());

//     const dataResponse = _.get(resultResponse, 'data');
//     if (!dataResponse) {
//       const error = new Error();
//       error.message = 'Bad request';
//       throw error;
//     }

//     yield put(actions.getListUserSuccess(dataResponse.data));
//   } catch (error) {
//     yield put(actions.throwError(error));
//   }
// }

// function* doGetUser(action) {
//   const { data } = action;

//   yield put(
//     actions.setAppLoading({
//       isLoading: true,
//       message: 'Login...',
//     }),
//   );

//   try {
//     const { posts, timeout } = yield race({
//       posts: call(Api.getUser, data),
//       timeout: delay(10000),
//     });

//     yield put(actions.setAppCancelLoading());

//     if (timeout) {
//       const error = new Error();
//       error.message = 'Request timeout!';
//       throw error;
//     }

//     const dataResponse = _.get(posts, 'data');
//     if (!dataResponse) {
//       const error = new Error();
//       error.message = 'Bad request';
//       throw error;
//     }

//     yield put(actions.getUserSuccess(dataResponse));
//   } catch (error) {
//     yield put(actions.throwError(error));
//   }
// }

export default function* watchUser() {
  // yield takeEvery(actionTypes.USER_GET_LIST, doGetListUser);
  // yield takeEvery(actionTypes.USER_GET, doGetUser);
}
