import { takeEvery, call, put, race, delay } from "redux-saga/effects";
import * as HttpStatus from "http-status-codes";
import _ from "lodash";

import * as positionAction from "../actions/positionAction";
import * as actions from "../actions";
import * as actionTypes from "../constants";
import { getList, addPosition, updatePosition, delPosition } from "../../services/firebase/positionFirebase";

function* doGetList() {
  try {
    const { posts, timeout } = yield race({
      posts: call(getList),
      timeout: delay(10000)
    });
    if (timeout) {
      const error = new Error();
      error.status = HttpStatus.REQUEST_TIMEOUT;
      throw error;
    }
    yield put(positionAction.getListSuccess(posts));
  } catch (error) {
    yield put(positionAction.getListFail());
    yield put(actions.throwError(error));
  }
}
function* doAdd(data) {
  try {
    const position = data.data;
    const { timeout } = yield race({
      posts: call(addPosition, position.name, position.description),
      timeout: delay(10000)
    });
    if (timeout) {
      const error = new Error();
      error.status = HttpStatus.REQUEST_TIMEOUT;
      throw error;
    }
    yield put(
      actions.openSnackbar({
        variant: "success",
        message: "Add successful",
        autoHideDuration: 2000
      })
    );
  } catch (error) {
    yield put(
      actions.openSnackbar({
        variant: "error",
        message: "Add Failed",
        autoHideDuration: 4000
      })
    );
    yield put(actions.throwError(error));
  }
}
function* doUpdate(data) {
  try {
    const position = data.data;
    const { timeout } = yield race({
      posts: call(updatePosition, position.id, position.name, position.description),
      timeout: delay(10000)
    });
    if (timeout) {
      const error = new Error();
      error.status = HttpStatus.REQUEST_TIMEOUT;
      throw error;
    }
    yield put(
      actions.openSnackbar({
        variant: "success",
        message: "Update successful",
        autoHideDuration: 2000
      })
    );
  } catch (error) {
    yield put(
      actions.openSnackbar({
        variant: "error",
        message: "Update Failed",
        autoHideDuration: 4000
      })
    );
    yield put(actions.throwError(error));
  }
}
function* doDelete(data) {
  try {
    const id = data.data;
    const { timeout } = yield race({
      posts: call(delPosition, id),
      timeout: delay(10000)
    });
    if (timeout) {
      const error = new Error();
      error.status = HttpStatus.REQUEST_TIMEOUT;
      throw error;
    }
    yield put(
      actions.openSnackbar({
        variant: "success",
        message: "Delete successful",
        autoHideDuration: 2000
      })
    );
  } catch (error) {
    yield put(
      actions.openSnackbar({
        variant: "error",
        message: "Delete Failed",
        autoHideDuration: 4000
      })
    );
    yield put(actions.throwError(error));
  }
}
export default function* watchPosition() {
  yield takeEvery(actionTypes.POSITION_GET_LIST, doGetList);
  yield takeEvery(actionTypes.POSITION_ADD, doAdd);
  yield takeEvery(actionTypes.POSITION_EDIT, doUpdate);
  yield takeEvery(actionTypes.POSITION_DEL, doDelete);
}
