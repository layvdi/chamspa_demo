import { takeEvery, call, put, race, delay } from "redux-saga/effects";
import * as HttpStatus from "http-status-codes";
import _ from "lodash";

import * as actions from "../actions";
import * as actionTypes from "../constants";
import { login } from "../../services/firebase/authFirebase";

function* doLogin(action) {
  const { data } = action;

  yield put(
    actions.setAppLoading({
      isLoading: true,
      message: "Login..."
    })
  );

  try {
    const { posts, timeout } = yield race({
      posts: call(login, data),
      timeout: delay(10000)
    });

    yield put(actions.setAppCancelLoading());

    if (timeout) {
      const error = new Error();
      error.status = HttpStatus.REQUEST_TIMEOUT;
      throw error;
    }

    yield put(
      actions.doLoginSuccess({
        email: posts.email,
        token: posts.token
      })
    );

    yield put(
      actions.openSnackbar({
        variant: "success",
        message: "Login successful",
        autoHideDuration: 2000
      })
    );
  } catch (error) {
    if (error.code) {
      yield put(
        actions.openSnackbar({
          variant: "error",
          message: "Wrong password or email",
          autoHideDuration: 4000
        })
      );
    }

    yield put(actions.throwError(error));
  }
}

export default function* watchAuth() {
  yield takeEvery(actionTypes.AUTH_DO_LOGIN, doLogin);
}
