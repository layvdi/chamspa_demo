import { takeEvery, call, put, race, delay } from "redux-saga/effects";
import * as HttpStatus from "http-status-codes";
import _ from "lodash";

import * as employeeAction from "../actions/employeeAction";
import * as actions from "../actions";
import * as actionTypes from "../constants";
import { getList,addEmployee ,updateEmployee,delEmployee} from "../../services/firebase/employeeFirebase";

function* doGetList() {
  try {
    const { posts, timeout } = yield race({
      posts: call(getList),
      timeout: delay(10000)
    });
    if (timeout) {
      const error = new Error();
      error.status = HttpStatus.REQUEST_TIMEOUT;
      throw error;
    }
    yield put(employeeAction.getListSuccess(posts));
  } catch (error) {
    yield put(employeeAction.getListFail());
    yield put(actions.throwError(error));
  }
}
function* doAdd(data) {
  try {
    const employee = data.data;
    const { timeout } = yield race({
      posts: call(addEmployee, employee.ref,employee.name,employee.birthday,employee.postision_id,employee.department_id),
      timeout: delay(10000)
    });
    if (timeout) {
      const error = new Error();
      error.status = HttpStatus.REQUEST_TIMEOUT;
      throw error;
    }
    yield put(
      actions.openSnackbar({
        variant: "success",
        message: "Add successful",
        autoHideDuration: 2000
      })
    );
  } catch (error) {
    yield put(
      actions.openSnackbar({
        variant: "error",
        message: "Add Failed",
        autoHideDuration: 4000
      })
    );
    yield put(actions.throwError(error));
  }
}
function* doUpdate(data) {
  try {
    const employee = data.data;
    const { timeout } = yield race({
      posts: call(updateEmployee, employee.ref,employee.name,employee.birthday,employee.postision_id,employee.department_id),
      timeout: delay(10000)
    });
    if (timeout) {
      const error = new Error();
      error.status = HttpStatus.REQUEST_TIMEOUT;
      throw error;
    }
    yield put(
      actions.openSnackbar({
        variant: "success",
        message: "Update successful",
        autoHideDuration: 2000
      })
    );
  } catch (error) {
    yield put(
      actions.openSnackbar({
        variant: "error",
        message: "Update Failed",
        autoHideDuration: 4000
      })
    );
    yield put(actions.throwError(error));
  }
}
function* doDelete(data) {
  try {
    const employee = data.data;
    const { timeout } = yield race({
      posts: call(delEmployee, employee.ref),
      timeout: delay(10000)
    });
    if (timeout) {
      const error = new Error();
      error.status = HttpStatus.REQUEST_TIMEOUT;
      throw error;
    }
    yield put(
      actions.openSnackbar({
        variant: "success",
        message: "Delete successful",
        autoHideDuration: 2000
      })
    );
  } catch (error) {
    yield put(
      actions.openSnackbar({
        variant: "error",
        message: "Delete Failed",
        autoHideDuration: 4000
      })
    );
    yield put(actions.throwError(error));
  }
}
export default function* watchEmployee() {
  yield takeEvery(actionTypes.EMPLOYEE_GET_LIST, doGetList);
  yield takeEvery(actionTypes.EMPLOYEE_ADD, doAdd);
  yield takeEvery(actionTypes.EMPLOYEE_EDIT, doUpdate);
  yield takeEvery(actionTypes.EMPLOYEE_DEL, doDelete);
}
