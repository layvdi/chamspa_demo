import { takeEvery, put } from "redux-saga/effects";
import * as HttpStatus from "http-status-codes";

import * as actions from "../actions";
import * as actionTypes from "../constants";

function* catchError(action) {
  const { data } = action;

  yield put(actions.setAppCancelLoading());

  if (data.status == HttpStatus.REQUEST_TIMEOUT) {
    yield put(
      actions.openSnackbar({
        variant: "warning",
        message: "Request timeout!",
        autoHideDuration: 3000
      })
    );
  }

  if (data.status == HttpStatus.UNAUTHORIZED) {
    yield put(
      actions.openSnackbar({
        variant: "error",
        message: "Incorrect login credentials. Please try again.",
        autoHideDuration: 4000
      })
    );
  }
}

export default function* watchApp() {
  yield takeEvery(actionTypes.THROW_ERROR, catchError);
}
