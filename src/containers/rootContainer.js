import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SnackbarContainer from './snackbarContainer';

class RootContainer extends Component {
  render() {
    return (
      <>
        <SnackbarContainer />
        {
          this.props.children
        }
      </>
    );
  }
}

RootContainer.propTypes = {
  children: PropTypes.object,
};

export default RootContainer;
