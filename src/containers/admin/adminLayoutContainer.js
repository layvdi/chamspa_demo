import { Component } from "react";
import { connect } from "react-redux";
import propTypes from "prop-types";
import Router from "next/router";

import * as actions from "../../stores/actions";
import AdminLayoutComponent from "../../components/commons/layouts/AdminLayoutComponent";

class AdminLayoutContainer extends Component {
  handleLogout = () => {
    this.props.doLogout();
  };

  render() {
    if (!process.browser) {
      return null;
    }

    if (!this.props.authReducer.token) {
      Router.push("/login");
      return null;
    }

    return (
      <AdminLayoutComponent handleLogout={this.handleLogout}>
        {this.props.children}
      </AdminLayoutComponent>
    );
  }
}

const mapStateToProps = state => ({
  authReducer: state.authReducer
});

const mapDispatchToProps = dispatch => ({
  doLogout: () => dispatch(actions.doLogout())
});

AdminLayoutContainer.propTypes = {
  children: propTypes.any.isRequired,
  authReducer: propTypes.object.isRequired,
  doLogout: propTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdminLayoutContainer);
