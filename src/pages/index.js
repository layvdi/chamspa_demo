import AdminLayoutContainer from "../containers/admin/adminLayoutContainer";
import HomeComponent from "../components/home/HomeComponent";

export default () => (
  <AdminLayoutContainer>
    <HomeComponent />
  </AdminLayoutContainer>
);
