import { useEffect } from "react";
import { useSelector, useDispatch, useStore } from "react-redux";
import dynamic from "next/dynamic";

import AdminLayoutContainer from "../containers/admin/adminLayoutContainer";
import * as departmentAction from "../stores/actions/departmentAction";

const DepartmentComponent = dynamic(() => import("../components/department/DepartmentComponent"), {
  ssr: false
});

const DepartmentPage = props => {
  const dispatch = useDispatch();
  const departmentReducer = useSelector(state => state.departmentReducer);

  const addDepartment = data => {
    dispatch(departmentAction.postAdd(data));
    dispatch(departmentAction.getList());
  };

  const editDepartment = data => {
    dispatch(departmentAction.putEdit(data));
    dispatch(departmentAction.getList());
  };

  const deleteDepartment = data => {
    dispatch(departmentAction.del(data));
    dispatch(departmentAction.getList());
  };

  useEffect(() => {
    dispatch(departmentAction.getList());
  }, []);

  return (
    <AdminLayoutContainer>
      <DepartmentComponent
        editDepartment={editDepartment}
        deleteDepartment={deleteDepartment}
        addDepartment={addDepartment}
        departmentReducer={departmentReducer}
      />
    </AdminLayoutContainer>
  );
};
export default DepartmentPage;
