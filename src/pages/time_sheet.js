import AdminLayoutContainer from "../containers/admin/adminLayoutContainer";
import TimeSheetComponent from "../components/timesheet/TimeSheetComponent";

export default () => (
  <AdminLayoutContainer>
    <TimeSheetComponent />
  </AdminLayoutContainer>
);
