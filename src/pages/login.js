import React, { Component } from "react";
import { connect } from "react-redux";
import propTypes from "prop-types";
import Router from "next/router";

import LoginComponent from "../components/loginComponent";
import * as actions from "../stores/actions";

class LoginPage extends Component {
  handleLogin = loginData => {
    this.props.doLogin(loginData);
  };

  render() {
    if (this.props.authReducer.token) {
      Router.push("/");
      return null;
    }

    return <LoginComponent handleLogin={this.handleLogin} appLoading={this.props.appLoading} />;
  }
}

LoginPage.propTypes = {
  doLogin: propTypes.func.isRequired,
  appLoading: propTypes.object.isRequired,
  authReducer: propTypes.object.isRequired
};

const mapDispatchToProps = dispatch => ({
  doLogin: data => dispatch(actions.doLogin(data))
});

const mapStateToProps = state => ({
  appLoading: state.appReducer.appLoading,
  authReducer: state.authReducer
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);
