import AdminLayoutContainer from "../containers/admin/adminLayoutContainer";

export default () => (
  <AdminLayoutContainer>
    <h1>Employee</h1>
  </AdminLayoutContainer>
);
