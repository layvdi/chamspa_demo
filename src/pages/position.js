import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import AdminLayoutContainer from "../containers/admin/adminLayoutContainer";
import * as positionAction from "../stores/actions/positionAction";
import * as helper from "../utils/helper";

import PositionComponent from "../components/position/PositionComponent";

const PositionPage = props => {
  const dispatch = useDispatch();

  const positions = useSelector(state => state.positionReducer.list);

  const positionConverted = helper.convertObjectToArray(positions, "id");

  useEffect(() => {
    dispatch(positionAction.getList());
  }, []);

  const addPosition = data => {
    dispatch(positionAction.postAdd(data));
    dispatch(positionAction.getList());
  };

  const editPosition = data => {
    dispatch(positionAction.putEdit(data));
    dispatch(positionAction.getList());
  };

  const delPosition = data => {
    dispatch(positionAction.del(data));
    dispatch(positionAction.getList());
  };

  return (
    <AdminLayoutContainer>
      <PositionComponent
        delPosition={delPosition}
        editPosition={editPosition}
        addPosition={addPosition}
        positions={positionConverted}
      ></PositionComponent>
    </AdminLayoutContainer>
  );
};
export default PositionPage;
