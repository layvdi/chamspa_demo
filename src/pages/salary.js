import AdminLayoutContainer from "../containers/admin/adminLayoutContainer";

export default () => (
  <AdminLayoutContainer>
    <h1>Salary</h1>
  </AdminLayoutContainer>
);
