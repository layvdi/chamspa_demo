import React from "react";
import App, { Container } from "next/app";
import Router from "next/router";
import Head from "next/head";
import { ThemeProvider } from "@material-ui/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import NProgress from "nprogress";
import { Provider } from "react-redux";
import withRedux from "next-redux-wrapper";

import store from "../stores";
import theme from "../utils/themeUtil";
import RootContainer from "../containers/rootContainer";
import "../static/styles/nprogress.css";

Router.events.on("routeChangeStart", () => {
  NProgress.start();
});

Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

class MyApp extends App {
  componentWillMount() {
    if (process.browser) {
      NProgress.start();
    }
  }

  componentDidMount() {
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentNode.removeChild(jssStyles);
    }

    if (process.browser) {
      NProgress.done();
    }
  }

  render() {
    const { Component, pageProps } = this.props;

    return (
      <Container>
        <Head>
          <title>HRM</title>
        </Head>
        <Provider store={store.store}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <RootContainer>
              <Component {...pageProps} />
            </RootContainer>
          </ThemeProvider>
        </Provider>
      </Container>
    );
  }
}

export default withRedux(() => store.store, { debug: false })(MyApp);
